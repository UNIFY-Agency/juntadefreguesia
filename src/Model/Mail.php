<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class sendMail{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public static function trySendMail($assunto, $destinatario, $html, $mail){

        if(empty($assunto) || empty($destinatario) || empty($html)){ return false; }

        // $mail = new PHPMailer(true);
        $mail->isSMTP();
        // $mail->SMTPDebug = SMTP::DEBUG_SERVER;
        $mail->SMTPAuth = true;
        $mail->Host = "smtp.office365.com";
        $mail->Port = 587;
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        // $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth   = true;
        $mail->Username = 'geral@eurotecnologia.pt';
        $mail->Password = 'QLLvHgkeAhzt694Q';
        $mail->SetFrom('geral@eurotecnologia.pt', 'Eurotecnologia');
        $mail->addAddress($destinatario, 'Cliente');
        // $mail->addAddress('renan.pantoja@hotmail.com', 'ToEmail');
        // $mail->SMTPDebug  = 3;
        // $mail->Debugoutput = function($str, $level) {echo "debug level $level; message: $str";};
        // $mail->Debugoutput = 'echo';
        // $mail->IsHTML(true);

        $mail->Subject = $assunto;
        $mail->Body    = $html;
        $mail->AltBody = 'Seu e-mail não recebe HTML contacte nossa empresa.';

        try
        {
        $mail->Send();
        // echo "Setting up PHPMailer with Office365 SMTP using php Success!";
        return true;
        }
        catch(Exception $exception)
        {
        echo "<pre>";
        var_dump($exception);
        echo "</pre>";
        //Something went bad
        echo "PHPMailer with Office365 Fail :: " . $mail->ErrorInfo;
        }

    }

    //CRUD

    public function insertCat($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO produtos_cat (nome) VALUES (:nome);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readCat($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE produtos_cat SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}




?>
